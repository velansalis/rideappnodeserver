const express = require("express");
const bodyParser = require("body-parser");

const router = require("./route/index");

const logger = require("./lib/shared/logging.handler");
const responseHandler = require("./lib/util/response.handler");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(logger.interceptor());

app.use("/api", router);

app.use(responseHandler.success);
app.use(responseHandler.error);

module.exports = app;
