module.exports = {
	auth: require("./auth/auth.module"),
	dealer: require("./dealer/dealer.module"),
	rider: require("./rider/rider.module"),
	service: require("./service/service.module"),
	socket: require("./socket/socket.module"),
	trip: require("./trip/trip.module"),
};
