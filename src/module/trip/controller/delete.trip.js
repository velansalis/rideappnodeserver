// Models
// Rider : This is the Rider's Model that is used to add data and fetch data from.
// Trip : This is the Trip Model that is used to get/add data from and to the trip model.
const { Rider, Trip } = require("../../../model/index");

// TODO:Priority3 Implement HttpException
const deleteTrip = async (req, res, next) => {
	/**
	 * @apiDefine DeleteTripParams
	 * @apiParam {String} tripID Trip ID of the trip
	 */
	let { tripID } = req.params;

	// Get the rider's ID from the decoded token. (Decoding token takes place in token handler (src/lib/shared/token.handler.js)
	// before reaching this controller and decoded token will be appended to req.token)
	let { _id } = req.token;

	try {
		// Remove the joinedTrips parameter from the rider's document
		// TODO:Priority1 When deleting the trip, delete trip from all rider's joinedTrips parameter.
		await Rider.findByIdAndUpdate({ _id }, { $pull: { joinedTrips: tripID } }).exec();

		// Delete the trip.
		// NOTE (To future self) : You can also make this unreadable instead of deleting by which you can undo deletion.
		await Trip.findOneAndDelete({ _id: tripID }).lean().exec();

		// Update the message that needs to be sent back to the user on successful transaction.
		let message = "trip successfully deleted";

		/**
		 * @apiDefine DeleteTripSuccess
		 * @apiSuccess {String} message Appropriate success message
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	message : "trip successfully deleted"
		 * }
		 */
		res.data = { message };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		next(err);
	}
};

module.exports = deleteTrip;
