// Models
// Trip : This is the Trip Model that is used to get/add data from and to the trip model.
const { Trip } = require("../../../model/index");

const getTrips = async (req, res, next) => {
	/**
	 * @apiDefine GetTripsParams
	 * @apiParam {String} page Page Number of the desired trips (pagination). eg. 1,2,3,4...
	 * @apiParam {String} sortby The service parameter with which the output needs to be sorted. eg. _id, createdAt, updatedAt
	 * @apiParam {String} orderby The order in which the output needs to be sorted. can be ascending (asc) or descending (dsc)
	 */
	let limit = 5;
	let page = req.query.page || 1;
	let startOffset = (page - 1) * limit;
	let endOffset = startOffset + limit;
	let sortby = req.query.sortby || "name";
	sortby = req.query.orderby == "asc" ? `+${sortby}` : `-${sortby}`;

	try {
		// Get the trips from Trip Model.
		let trips = await Trip.find({}).sort(sortby).skip(startOffset).limit(endOffset).select("name startDate endDate").lean().exec();

		/**
		 * @apiDefine GetTripsSuccess
		 * @apiSuccess {Number} count Number of trips returned
		 * @apiSuccess {String} _id Generated Unique ID of the trip
		 * @apiSuccess {String} name Name of the trip
		 * @apiSuccessExample {JSON} Success Response
		 * data : {
		 * 	count : 1,
		 * 	trips : [
		 * 		"_id": "5ead17c809369b4b1d57a481",
		 * 		"name": "Exquisite Manali",
		 * 		"startDate" : <startDate>,
		 * 		"endDate" : <endDate>
		 * 	]
		 * }
		 */
		res.data = { count: trips.length, trips };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		next(err);
	}
};

module.exports = getTrips;
