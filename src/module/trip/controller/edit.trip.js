// Models
// Trip : This is the Trip Model that is used to get/add data from and to the trip model.
const { Trip } = require("../../../model/index");

// Types
// HttpException : The Exception that needs to be thrown when some exception occurs in the code
const { HttpException } = require("../../../types/index");

/**
 * This function prevents the user from updating the non-updatable fields.
 * @param {RequestBody} updateQuery This is the object containing the request body.
 */
const validateUpdateQuery = (updateQuery) => {
	let nonUpdatableFields = ["_id", "owner"];
	let updateQueryFields = new Set(Object.keys(updateQuery));

	return new Promise((resolve, reject) => {
		if (updateQueryFields.size == 0) reject(new HttpException("BadSyntaxField", "update query is required"));
		nonUpdatableFields.map((value) => {
			if (updateQueryFields.has(value)) reject(new HttpException("UnauthorizedAccess", `${value} can't be updated explicitely`));
		});
		resolve();
	});
};

const editTrip = async (req, res, next) => {
	/**
	 * @apiDefine EditTripParams
	 * @apiParam {JSON} query Update query for the trip.
	 * @apiParamExample  {JSON} Request Body
	 * {
	 * 	"name": "Exquisite Manali",
	 * 	"destination": [100, -200],
	 * 	"source": [300,200],
	 * 	... other required parameters ...
	 * }
	 */
	const { body } = req;

	// Get the Trip ID from URL parameter.
	// This ID will be used to fetch the trip from Trip Model.
	const { tripID } = req.params;

	try {
		// Validate the updatequery to throw error when attempting to update the non updatable fields.
		await validateUpdateQuery(body);

		// Update the trip with valid update parameters. This function will return the updated rider parameters.
		await Trip.findOneAndUpdate({ _id: tripID }, body, {
			new: true,
			runValidators: true,
		})
			.lean()
			.exec();

		// Update the message that needs to be sent back to the user on successful transaction.
		let message = "trip successfully updated";

		/**
		 * @apiDefine EditTripSuccess
		 * @apiSuccess {String} message Appropriate success message
		 * @apiSuccessExample {JSON} Success Response
		 * data : {
		 * 	message : "trip successfully updated"
		 * }
		 */
		res.data = { message };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		// NOTE : ValidationError is occured from the Database Model's side.
		if (err.name == "ValidationError") err = new HttpException("BadSyntaxField", err.message);
		next(err);
	}
};

module.exports = editTrip;
