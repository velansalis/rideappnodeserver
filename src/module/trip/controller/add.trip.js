// Models
// Rider : This is the Rider's Model that is used to add data and fetch data from.
// Trip : This is the Trip Model that is used to get/add data from and to the trip model.
const { Rider, Trip } = require("../../../model/index");

// Types
// HttpException : The Exception that needs to be thrown when some exception occurs in the code
const { HttpException } = require("../../../types/index");

// Handlers
// timestampHandler : This handler handles conversion and comparision of timestamps.
const { timestampHandler } = require("../../../lib/index").shared;

const addTrip = async (req, res, next) => {
	/**
	 * @apiDefine AddTripParams
	 * @apiParam {String} name Name of the trip
	 * @apiParam {Array} source Trip source in latitudes and longitude values.
	 * @apiParam {Array} destination Trip destination in latitudes and longitude values.
	 * @apiParam {Array} milestones Trip milestones in latitudes and longitude values.
	 * @apiParam {String} startDate Trip start date in DD/MM/YYYY format
	 * @apiParam {String} startTime Trip start time in hh:mm format
	 * @apiParam {String} endDate Trip end date in DD/MM/YYYY format
	 * @apiParam {Array} invitedUsers Profile ID's of riders to be invited.
	 * @apiParamExample  {JSON} Request Example:
	 * {
	 * 	"name": "Exquisite Manali",
	 * 	"destination": [100, -200],
	 * 	"source": [300,200],
	 * 	"startDate": "1/2/1997",
	 * 	"endDate": "2/2/1997",
	 * 	"startTime": "10:30",
	 * 	"milestones": [[100,200],[300,400],[500,600]],
	 * 	"invitedUsers": [ <"Rider profile ID's"> ]
	 * }
	 */
	let { name, source, destination, startDate, startTime, endDate, invitedUsers, milestones } = req.body;

	// Get the rider's ID from the decoded token. (Decoding token takes place in token handler (src/lib/shared/token.handler.js)
	// before reaching this controller and decoded token will be appended to req.token)
	let owner = req.token._id;

	try {
		// Convert the start date and end date to computable parameters and store them in their
		// respective variables.
		startDate = await timestampHandler.formatDateAndTime(startDate, startTime);
		endDate = await timestampHandler.formatDate(endDate);

		// If start date is greater than end date, then the dates are invalid. Throw error.
		if (startDate > endDate) throw new HttpException("BadSyntaxField", "start date should be before end date");

		// Convert the computable date object to an ISO string that makes it easy and lightweight to store
		startDate = startDate.toISOString();
		endDate = endDate.toISOString();

		// List all the required parameters and create the trip.
		let createData = { name, destination, source, startDate, endDate, invitedUsers, milestones, owner };
		let trip = await Trip.create(createData);

		// Update the rider's document joinedTrips with Trip ID of the created Trip which will indicate the rider is a
		// part of this trip when fetching rider's information.
		await Rider.findOneAndUpdate({ _id: owner }, { $push: { joinedTrips: trip._id } })
			.lean()
			.exec();

		// Update the message that needs to be sent back to the user on successful transaction.
		let message = "trip successfully added";

		/**
		 * @apiDefine AddTripSuccess
		 * @apiSuccess {String} message Appropriate success message
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	message : "trip successfully added"
		 * }
		 */
		res.data = { message };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		// NOTE : ValidationError is occured from the Database Model's side.
		if (err.name == "ValidationError") err = new HttpException("BadSyntaxField", err.message);
		next(err);
	}
};

module.exports = addTrip;
