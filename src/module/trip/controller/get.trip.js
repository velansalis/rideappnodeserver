// Models
// Trip : This is the Trip Model that is used to get/add data from and to the trip model.
const { Trip } = require("../../../model/index");

const getTrip = async (req, res, next) => {
	/**
	 * @apiDefine GetTripParams
	 * @apiParam {String} tripID Trip ID of the trip
	 */
	let { tripID } = req.params;

	// Create the getQuery through which the data from trips will be fetched.
	let getQuery = { _id: tripID };

	// These are the fields that will be fetched from the server when requesting individual trip.
	let selectQuery = "name destination source startDate endDate milestones invitedUsers";

	try {
		// Fetch the trip through getQuery and then store it in the trip variable.
		let trip = await Trip.findOne(getQuery).lean().select(selectQuery).exec();

		// TODO:Priority3 Implement 'if trip not exists' check.

		/**
		 * @apiDefine GetTripSuccess
		 * @apiSuccess {String} _id Generated Unique ID of the trip
		 * @apiSuccess {String} name Name of the trip
		 * @apiSuccess {Array} source Trip source in latitudes and longitude values.
		 * @apiSuccess {Array} destination Trip destination in latitudes and longitude values.
		 * @apiSuccess {Array} milestones Trip milestones in latitudes and longitude values.
		 * @apiSuccess {String} startDate Trip start date and start time in UTC format
		 * @apiSuccess {String} endDate Trip end date in UTC format
		 * @apiSuccess {Array} invitedUsers Profile ID's of riders invited.
		 * @apiSuccessExample {JSON} Success Response:
		 * {
		 * 	data : {
		 * 		"_id": "5ead17c809369b4b1d57a481",
		 * 		"destination": [100,-200],
		 * 		"source": [300,200],
		 * 		"milestones": [[100,200],[300,400],[500,600]],
		 * 		"invitedUsers": [],
		 * 		"name": "Exquisite Manali",
		 * 		"startDate": "1997-02-01T05:00:00.000Z",
		 * 		"endDate": "1997-02-01T18:30:00.000Z"
		 * 	}
		 * }
		 */
		res.data = trip;
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		next(err);
	}
};

module.exports = getTrip;
