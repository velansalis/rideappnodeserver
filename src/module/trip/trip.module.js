module.exports = {
	/**
	 * @api {get} {BASE_URL}/api/trip/<tripID> Gets Trip By ID
	 * @apiName Get trip by ID
	 * @apiDescription This module returns the trip associated with the trip ID in the URL query.
	 * @apiGroup Trip
	 * @apiVersion  1.0.0
	 * @apiPermission Rider
	 * @apiUse BearerToken
	 * @apiUse GetTripParams
	 * @apiUse GetTripSuccess
	 * @apiUse ErrorResponse
	 */
	getTrip: require("./controller/get.trip"),

	/**
	 * @api {get} {BASE_URL}/api/trip?page=<pagenumber>&&sortBy=<params>&&orderBy=<orderby> Get Trips
	 * @apiName Get Trips
	 * @apiDescription This module returns the trips based on provided URL query filters. (filters are optional, yet they
	 * can be used to have greater control over the trips response)
	 * @apiGroup Trip
	 * @apiVersion 1.0.0
	 * @apiPermission Rider
	 * @apiUse BearerToken
	 * @apiUse GetTripsParams
	 * @apiUse GetTripsSuccess
	 * @apiUse ErrorResponse
	 */
	getTrips: require("./controller/get.trips"),

	/**
	 * @api {post} {BASE_URL}/api/trip Add Trip
	 * @apiName Add trip
	 * @apiDescription This module authenticates the user and creates a trip on his/her ownership. And adds the created trip
	 * ID in user's record under 'joinedTrips' parameter.
	 * @apiGroup Trip
	 * @apiVersion 1.0.0
	 * @apiPermission Rider
	 * @apiUse BearerToken
	 * @apiUse AddTripParams
	 * @apiUse AddTripSuccess
	 * @apiUse ErrorResponse
	 */
	addTrip: require("./controller/add.trip"),

	/**
	 * @api {put} {BASE_URL}/api/trip/:tripID Edit Trip
	 * @apiDescription This module updates the trip parameters.
	 * @apiName Edit trip
	 * @apiGroup Trip
	 * @apiVersion  1.0.0
	 * @apiPermission Trip Owner
	 * @apiUse BearerToken
	 * @apiUse EditTripParams
	 * @apiUse EditTripSuccess
	 * @apiUse ErrorResponse
	 */
	editTrip: require("./controller/edit.trip"),

	/**
	 * @api {delete} {BASE_URL}/api/trip/<tripID> Delete Trip
	 * @apiName Delete trip
	 * @apiDescription This module authenticates the user and deletes the trip. Removes the trip ID from user's 'joinedTrips' parameter.
	 * @apiGroup Trip
	 * @apiVersion  1.0.0
	 * @apiPermission Trip Owner
	 * @apiUse BearerToken
	 * @apiUse DeleteTripParams
	 * @apiUse DeleteTripSuccess
	 * @apiUse ErrorResponse
	 */
	deleteTrip: require("./controller/delete.trip"),
};
