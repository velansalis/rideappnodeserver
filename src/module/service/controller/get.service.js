const { Service } = require("../../../model/index");

const { HttpException } = require("../../../types/index");

const getService = async (req, res, next) => {
	/**
	 * @apiDefine GetServiceParams
	 * @apiParam {String} serviceID ID of the service
	 * @apiParam {String} populate This field decides if the dealer details should be fetched or not. 'true' indicates dealers need to be fetched. Default value is false.
	 */
	let { serviceID } = req.params;
	let { populate } = req.query;
	let getQuery = { _id: serviceID };

	try {
		let populateField = "";
		if (populate.toLowerCase() == "true") {
			populateField = "dealer";
		}

		// TODO:Priority3 (Improve readability and add tests to make sure services added fine)
		let service = await Service.findOne(getQuery).populate(populateField, "_id name city").lean().exec();
		if (!service) throw new HttpException("BadSyntaxField", "service doesn't exists");

		/**
		 * @apiDefine GetServiceSuccess
		 * @apiSuccess {String} message Appropriate success message
		 * @apiSuccessExample {JSON} Success Response:
		 * {
		 * 	data : {
		 * 		message : "dealer successfully created"
		 * 	}
		 * }
		 */
		res.data = service;
		next();
	} catch (err) {
		res.status(403);
		err = new Error(err.message);
		next(err);
	}
};

module.exports = getService;
