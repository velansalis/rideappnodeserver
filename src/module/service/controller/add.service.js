const { Service, Dealer, Rider } = require("../../../model/index");

const { timestampHandler } = require("../../../lib/index").shared;

const { HttpException } = require("../../../types/index");

const addService = async (req, res, next) => {
	/**
	 * @apiDefine AddServiceParams
	 * @apiParam {String} phone Phone number of the rider
	 * @apiParam {Date} slotDate Slot date of the service
	 * @apiParam {Date} slotTime Slot time of the service (use 24 hour format eg. 12 AM becomes 00:00)
	 * @apiParam {String} vehicleNumber Vehicle number of rider's vehicle
	 * @apiParam {String} vehicleType Vehicle type of rider's vehicle
	 * @apiParam {String} serviceType Type of service [General, Free, Breakdown]
	 * @apiParam {String} comments Comments (tags) of services
	 * @apiParam {Number} dealer ID of the dealer
	 * @apiParamExample {JSON} Request Body
	 * {
	 * 	phone : 9876543210,
	 * 	slotDate : "1/2/2019",
	 * 	slotTime : "10:30"
	 * 	vehicleNumber : KA 02 F 9087,
	 * 	vehicleType : Classic 350 - black,
	 * 	serviceType : general,
	 * 	comments : "break oil, tight chain",
	 * 	dealer : 5eae5f8d1182562facc4f929
	 * }
	 */
	let { phone, slotDate, slotTime, vehicleType, vehicleNumber, serviceType, comments, dealer } = req.body;
	let { _id } = req.token;

	try {
		// TODO:Priority2 (Further tests to improve readability and efficiency)
		let dealerExists = await Dealer.findOne({ _id: dealer });
		if (!dealerExists) throw new HttpException("BadSyntaxField", "dealer doesn't exists");

		let slot = await timestampHandler.formatDateAndTime(slotDate, slotTime);
		slot = slot.toISOString();

		let data = { phone, slot, vehicleType, vehicleNumber, serviceType, comments, dealer, owner: _id };
		let service = await Service.create(data);

		await Rider.findOneAndUpdate({ _id }, { $push: { services: service._id } })
			.lean()
			.exec();

		/**
		 * @apiDefine AddServiceSuccess
		 * @apiSuccess {String} message Appropriate success message
		 * @apiSuccessExample {JSON} Success Response:
		 * {
		 * 	data : {
		 * 		message : "dealer successfully created"
		 * 	}
		 * }
		 */
		res.data = { message: "service successfully added" };
		next();
	} catch (err) {
		if (err.name == "ValidationError") err = new HttpException("BadSyntaxField", err.message);
		next(err);
	}
};

module.exports = addService;
