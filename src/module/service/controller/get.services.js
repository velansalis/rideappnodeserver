const { Service, Rider } = require("../../../model/index");

const { HttpException } = require("../../../types/index");

const getServices = async (req, res, next) => {
	/**
	 * @apiDefine GetServicesParams
	 * @apiParam {String} page Page Number of the desired trips (pagination). eg. 1,2,3,4...
	 * @apiParam {String} sortby The service parameter with which the output needs to be sorted. eg. name, createdAt, updatedAt
	 * @apiParam {String} orderby The order in which the output needs to be sorted. can be ascending (asc) or descending (dsc)
	 */
	let { _id } = req.token;

	let limit = 5;
	let page = req.query.page || 1;
	let startOffset = (page - 1) * limit;
	let endOffset = startOffset + limit;
	let sortby = req.query.sortby || "name";
	sortby = req.query.orderby == "asc" ? `+${sortby}` : `-${sortby}`;

	try {
		// Check if the rider exists by passing the populated '_id'
		// If the rider doesn't exists, throw error
		let rider = await Rider.findOne({ _id }).exec();
		if (!rider) throw new HttpException("UserNotExists", "rider doesn't exists");

		// Fetch all the services that are owned by the rider
		let services = await Service.find({ owner: _id })
			.sort(sortby)
			.skip(startOffset)
			.limit(endOffset)
			.select("_id slot vehicleType serviceType")
			.lean()
			.exec();

		/**
		 * @apiDefine GetServicesSuccess
		 * @apiSuccess {String} _id ID of the service (which can be used to fetch more details about a single service)
		 * @apiSuccess {String} slot Slot time information timestamp in UTC
		 * @apiSuccess {String} vehicleType Vehicle Type
		 * @apiSuccess {String} serviceType Service Type
		 * @apiSuccessExample {JSON} Success Response:
		 * {
		 * 	data : {
		 *      "_id": "5f3a34f0064eeb0d2ba50509",
		 *      "slot": "2019-02-02T05:00:00.000Z",
		 *      "vehicleType": "Classic 350 - black",
		 *      "serviceType": "general"
		 *  }
		 * }
		 */
		res.data = services;
		next();
	} catch (err) {
		res.status(403);
		err = new Error(err.message);
		next(err);
	}
};

module.exports = getServices;
