module.exports = {
	/**
	 * @api {post} {BASE_URL}/api/service Add Service
	 * @apiName Add a Service
	 * @apiDescription This module adds a vehicle service.
	 * @apiGroup Service
	 * @apiVersion 1.0.0
	 * @apiPermission Rider
	 * @apiUse BearerToken
	 * @apiUse AddServiceParams
	 * @apiUse AddServiceSuccess
	 * @apiUse ErrorResponse
	 */
	addService: require("./controller/add.service"),

	/**
	 * @api {get} {BASE_URL}/api/service/:serviceID?populate=<true|false> Get Service
	 * @apiName Get Service Details
	 * @apiDescription This module gets vehicle service details from ID.
	 * @apiGroup Service
	 * @apiVersion  1.0.0
	 * @apiPermission Rider
	 * @apiUse BearerToken
	 * @apiUse GetServiceParams
	 * @apiUse GetServiceSuccess
	 * @apiUse ErrorResponse
	 */
	getService: require("./controller/get.service"),

	/**
	 * @api {get} {BASE_URL}/api/service?page=<pagenumber>&&sortBy=<params>&&orderBy=<orderby> Get Services
	 * @apiName Get Services
	 * @apiDescription This module lists all the services that are created by the user along with their id's
	 * @apiGroup Service
	 * @apiVersion  1.0.0
	 * @apiPermission Rider
	 * @apiUse BearerToken
	 * @apiUse GetServicesParams
	 * @apiUse GetServicesSuccess
	 * @apiUse ErrorResponse
	 */
	getServices: require("./controller/get.services"),
};
