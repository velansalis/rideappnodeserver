const { Rider } = require("../../../model/index");

const addAvatar = async (req, res, next) => {
	/**
	 * @apiDefine AddAvatarParams
	 * @apiParam {JSON} avatar Image file need to be uploaded in multipart form
	 * @apiParamExample {JSON} Parameter Example
	 * { avatar : ImageFile }
	 */
	const getQuery = { _id: req.token };
	const updateQuery = { avatar: req.file };

	try {
		// TODO
		// Write more tests to test if the image is updatable before updating.
		// This can be written in file upload handler.
		await Rider.findOneAndUpdate(getQuery, updateQuery).lean().exec();

		/**
		 * @apiDefine AddAvatarSuccess
		 * @apiSuccess {String} message Appropriate success message
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	message : "avatar successfully uploaded"
		 * }
		 */
		res.data = { message: "avatar successfully uploaded" };
		next();
	} catch (err) {
		res.status(403);
		err = new Error(err.message);
		next(err);
	}
};

module.exports = addAvatar;
