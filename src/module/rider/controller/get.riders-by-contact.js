const { Rider } = require("../../../model/index");

const getRider = async (req, res, next) => {
	/**
	 * @apiDefine GetRiderByContactParams
	 * @apiParam Array(String) contactList This section should contain an array that has the phone numbers of the riders.
	 */
	let { contactList } = req.body;
	let selectQuery = "name email phone avatar joinedTrips services status createdAt updatedAt";

	try {
		let ridersUsingRideapp = [];
		for (contact of contactList) {
			rider = await Rider.findOne({ phone: contact }).select(selectQuery).lean().exec();
			if (rider) ridersUsingRideapp.concat({ _id: rider._id, contact });
		}

		/**
		 * @apiDefine GetRiderByContactSuccess
		 * @apiSuccess {String} _id Rider ID
		 * @apiSuccess {String} contact Contact Number of the Rider
		 * @apiSuccessExample {json} Success Response
		 * "data": [{
		 * 	"_id": "5eae80d77fd66c5610fbbf26",
		 * 	"contact": 9876543210,
		 * }]
		 */
		res.data = ridersUsingRideapp;
		next();
	} catch (err) {
		next(err);
	}
};

module.exports = getRider;
