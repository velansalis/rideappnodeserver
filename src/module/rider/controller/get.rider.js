const { Rider } = require("../../../model/index");

const { HttpException } = require("../../../types/index");

const getRider = async (req, res, next) => {
	/**
	 * @apiDefine GetRiderParams
	 * @apiParam {String} _id Rider ID
	 * @apiParamExample {URL} Example URL
	 * /api/rider (for self profile)
	 * /api/rider/_id=5eae80d77fd66c5610fbbf26 (for other user's profile)
	 */
	let _id = req.query._id || req.token._id;
	let rider = null;
	let getQuery = { _id };
	let selectQuery = "name email phone avatar joinedTrips services status createdAt updatedAt";

	try {
		rider = await Rider.findOne(getQuery).select(selectQuery).lean().exec();
		if (!rider) throw new HttpException("UserNotExists", "rider doesn't exists");

		/**
		 * @apiDefine GetRiderSuccess
		 * @apiSuccess {String} _id Rider ID
		 * @apiSuccess {String} name Rider name
		 * @apiSuccess {String} email Rider email
		 * @apiSuccess {String} phone Rider phone number
		 * @apiSuccess {String} avatar Rider's avatar name
		 * @apiSuccess {Array} joinedTrips List of ID's of trips joined or created by the user.
		 * @apiSuccess {Array} services List of ID's of the services done by the user.
		 * @apiSuccess {String} status Online/Offline status
		 * @apiSuccess {Date} createdAt Timestamp of when the rider was created
		 * @apiSuccess {Date} updatedAt Timestamp of when the rider was last updated
		 * @apiSuccessExample {json} Success Response
		 * "data": {
		 * 	"_id": "5eae80d77fd66c5610fbbf26",
		 * 	"avatar": null,
		 * 	"joinedTrips": [],
		 * 	"services": [],
		 * 	"status": "offline",
		 * 	"name": "Velan Salis",
		 * 	"email": "velansalis@tuta.io",
		 * 	"phone": "9876543230",
		 * 	"createdAt": "2020-05-03T08:29:11.391Z",
		 * 	"updatedAt": "2020-05-03T08:29:11.395Z"
		 * }
		 */
		res.data = rider;
		next();
	} catch (err) {
		next(err);
	}
};

module.exports = getRider;
