const { Rider } = require("../../../model/index");

const { HttpException } = require("../../../types/index");

/**
 * This function prevents the user from updating the non-updatable fields.
 * @param {RequestBody} updateQuery This is the object containing the request body.
 */
const validateUpdateQuery = (updateQuery) => {
	let nonUpdatableFields = ["_id", "joinedTrips", "services", "avatar", "password", "token", "socket", "status"];
	let updateQueryFields = new Set(Object.keys(updateQuery));

	return new Promise((resolve, reject) => {
		if (updateQueryFields.size == 0) reject(new HttpException("BadSyntaxField", "update query is required"));
		nonUpdatableFields.map((value) => {
			if (updateQueryFields.has(value)) reject(new HttpException("UnauthorizedAccess", `${value} can't be updated explicitely`));
		});
		resolve();
	});
};

const editRider = async (req, res, next) => {
	/**
	 * @apiDefine EditRiderParams
	 * @apiParam {JSON} body Update Query
	 * @apiParamExample {JSON} Parameter Example
	 * {
	 * 	name : "Jane Doe",
	 * 	phone : 9876543210
	 * 	...required update queries...
	 * }
	 */
	let getQuery = { _id: req.token };
	let updateQuery = req.body;

	try {
		await validateUpdateQuery(updateQuery);
		await Rider.findOneAndUpdate(getQuery, updateQuery, { new: true, runValidators: true }).exec();

		/**
		 * @apiDefine EditRiderSuccess
		 * @apiSuccess {String} message Appropriate success message
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	message : "rider successfully updated"
		 * }
		 */
		res.data = { message: "rider successfully updated" };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		// NOTE : ValidationError is occured from the Database Model's side.
		if (err.name == "ValidationError") err = new HttpException("BadSyntaxField", err.message);
		next(err);
	}
};

module.exports = editRider;
