module.exports = {
	/**
	 * @api {get} {BASE_URL}/api/rider?_id=<riderID> Get Rider Profile
	 * @apiName Get Rider
	 * @apiGroup Rider
	 * @apiVersion  1.0.0
	 * @apiPermission Rider
	 * @apiDescription his module takes in rider ID from the URL parameter and sends the rider information back to the
	 * user. If URL parameter is not passed, current rider information is returned.
	 * @apiUse BearerToken
	 * @apiUse GetRiderParams
	 * @apiUse GetRiderSuccess
	 * @apiUse ErrorResponse
	 */
	getRider: require("./controller/get.rider"),

	/**
	 * @api {put} {BASE_URL}/api/rider Update Rider Profile
	 * @apiName Update Rider Profile
	 * @apiDescription This module updates the parameters in rider's document.
	 * @apiGroup Rider
	 * @apiVersion  1.0.0
	 * @apiPermission Rider (Owner)
	 * @apiUse BearerToken
	 * @apiUse EditRiderParams
	 * @apiUse EditRiderSuccess
	 * @apiUse ErrorResponse
	 */
	editRider: require("./controller/edit.rider"),

	/**
	 * @api {post} {BASE_URL}/api/rider/avatar Add profile picture
	 * @apiName Add profile picture
	 * @apiDescription This module uploads the profile picture of the rider and adds the file name to the 'avatar' parameter in rider's document.
	 * @apiGroup Rider
	 * @apiVersion 1.0.0
	 * @apiPermission Rider (Owner)
	 * @apiUse BearerToken
	 * @apiUse AddAvatarParams
	 * @apiUse AddAvatarSuccess
	 * @apiUse ErrorResponse
	 */
	addAvatar: require("./controller/add.avatar"),

	/**
	 * @api {get} {BASE_URL}/api/rider/contacts Get Rider By Contacts
	 * @apiName Get Rider By Contacts
	 * @apiDescription This module gets an Array of contact numbers and returns an Array of contact numbers and associated ID's of Rideapp users.
	 * @apiGroup Rider
	 * @apiVersion 1.0.1
	 * @apiPermission Rider (Owner)
	 * @apiUse BearerToken
	 * @apiUse GetRiderByContactParams
	 * @apiUse GetRiderByContactSuccess
	 * @apiUse ErrorResponse
	 */
	getRidersByContact: require("./controller/get.riders-by-contact"),
};
