const { Dealer } = require("../../../model/index");

const { HttpException } = require("../../../types/index");

const addDealer = async (req, res, next) => {
	/**
	 * @apiDefine AddDealerParams
	 * @apiParam {String} name Name of the dealer
	 * @apiParam {String} address Address of the dealer
	 * @apiParam {String} city Name of the City
	 * @apiParam {String} state Name of the state
	 * @apiParam {String} phone Phone number of the dealer
	 * @apiParam {Number} ratings Rating of the dealer
	 * @apiParamExample {JSON} Request Body
	 *	{
	 * 		"name": "Tune Motors",
	 * 		"address": "NH 6, Edapally, Santhekatte",
	 * 		"city": "udupi",
	 * 		"state": "karnataka",
	 * 		"phone": "9876543210",
	 * 		"ratings": 4
	 *	}
	 */
	let { name, address, city, state, phone, ratings } = req.body;
	let addQuery = { name, address, city, state, phone, ratings };

	try {
		let dealer = await Dealer.create(addQuery);

		/**
		 * @apiDefine AddDealerSuccess
		 * @apiSuccess {String} message Appropriate success message
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	message : "dealer successfully created"
		 * }
		 */
		res.data = { message: "dealer successfully created" };
		next();
	} catch (err) {
		if (err.name == "ValidationError") err = new HttpException("BadSyntaxField", err.message);
		next(err);
	}
};

module.exports = addDealer;
