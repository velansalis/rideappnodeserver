const { Dealer } = require("../../../model/index");

const getDealers = async (req, res, next) => {
	/**
	 * @apiDefine GetDealerParams
	 * @apiParam {String} :dealerID ID of the dealer
	 * @apiParamExample {URL} Example URL
	 * /api/dealer/5eae5f8d1182562facc4f929
	 */
	let { dealerID } = req.params;
	let selectQuery = "name ratings address state city phone";

	try {
		let dealer = await Dealer.findOne({ _id: dealerID }).select(selectQuery).lean().exec();

		/**
		 * @apiDefine GetDealerSuccess
		 * @apiSuccess {String} _id ID of the dealer
		 * @apiSuccess {String} name Dealer name
		 * @apiSuccess {String} address Dealer address
		 * @apiSuccess {String} phone Dealer's phone number
		 * @apiSuccess {String} city Dealer's city
		 * @apiSuccess {String} state Dealer's state
		 * @apiSuccess {Number} ratings Dealer's rating.
		 * @apiSuccessExample {json} Success Response:
		 * {
		 * 	"data": {
		 * 		"_id": "5eae5f8d1182562facc4f929",
		 * 		"name": "Tune Motors",
		 * 		"address": "NH 6, Edapally, Santhekatte",
		 * 		"phone" : "9876543210",
		 * 		"city": "udupi",
		 * 		"state": "karnataka",
		 * 		"ratings": 4
		 * 	}
		 * }
		 */
		res.data = dealer;
		next();
	} catch (err) {
		next(err);
	}
};

module.exports = getDealers;
