const { Dealer } = require("../../../model/index");

const getDealers = async (req, res, next) => {
	/**
	 * @apiDefine GetDealersParams
	 * @apiParam {String} city City name
	 * @apiParam {Number} page Page number
	 * @apiParam {String} sortby Parameter by which output needs to be sorted
	 * @apiParam {String} orderby Ascending or Descending (asc or dsc)
	 * @apiParamExample {URL} Example URL
	 * /api/dealer?city=udupi
	 */
	let limit = 5;
	let page = req.query.page || 1;
	let data = {};
	req.query.city ? (data["city"] = req.query.city) : undefined;

	let startOffset = (page - 1) * limit;
	let endOffset = startOffset + limit;
	let sortby = req.query.sortby || "name";
	sortby = req.query.orderby == "asc" ? `+${sortby}` : `-${sortby}`;

	let selectQuery = "name ratings city phone";

	try {
		let dealers = await Dealer.find(data).sort(sortby).skip(startOffset).limit(endOffset).select(selectQuery).lean().exec();

		/**
		 * @apiDefine GetDealersSuccess
		 * @apiSuccess {String} _id ID of the dealer (which will be useful when creating the service)
		 * @apiSuccess {String} name Dealer's name
		 * @apiSuccess {String} city Dealer's city
		 * @apiSuccess {String} phone Dealer's phone number
		 * @apiSuccess {String} ratings Dealer's ratings
		 * @apiSuccessExample {json} Success Response:
		 * {
		 * 	"data": {
		 * 		"count": 1,
		 * 		"dealers": [
		 * 			{
		 * 				_id": "5eae5f8d1182562facc4f929",
		 * 				"name": "Tune Motors",
		 * 				"city": "udupi",
		 * 				"phone" : "9876543210",
		 * 				"ratings": 4
		 * 			},
		 * 		}
		 * 	}
		 * }
		 */
		res.data = { count: dealers.length, dealers };
		next();
	} catch (err) {
		next(err);
	}
};

module.exports = getDealers;
