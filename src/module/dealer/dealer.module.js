// TODO:Priority1 (Standardize all below modules)
module.exports = {
	/**
	 * @api {post} {BASE_URL}/api/dealer Add Dealer
	 * @apiName Add a Dealer
	 * @apiDescription This module adds a service dealer.
	 * @apiGroup Dealer
	 * @apiVersion  1.0.0
	 * @apiPermission Rider
	 * @apiUse BearerToken
	 * @apiUse AddDealerParams
	 * @apiUse AddDealerSuccess
	 * @apiUse ErrorResponse
	 */
	addDealer: require("./controller/add.dealer"),

	/**
	 * @api {get} {BASE_URL}/api/dealer?city=<cityName>&&page=<pageNumber>&&sortby=<sortParameter>&&orderBy<orderParameter> Get Dealers
	 * @apiName Get Dealers
	 * @apiDescription This module gets the list of dealers filtered by URL parameters.
	 * @apiGroup Dealer
	 * @apiVersion  1.0.0
	 * @apiPermission Rider
	 * @apiUse BearerToken
	 * @apiUse GetDealersParams
	 * @apiUse GetDealersSuccess
	 * @apiUse ErrorResponse
	 */
	getDealers: require("./controller/get.dealers"),

	/**
	 * @api {get} {BASE_URL}/api/dealer/:dealerID Get Dealer
	 * @apiName Get Dealer
	 * @apiDescription This module gets the dealer.
	 * @apiGroup Dealer
	 * @apiVersion  1.0.0
	 * @apiPermission Rider
	 * @apiUse BearerToken
	 * @apiUse GetDealerParams
	 * @apiUse GetDealerSuccess
	 * @apiUse ErrorResponse
	 */
	getDealer: require("./controller/get.dealer"),
};
