// Models
// Rider : This is the Rider's Model that is used to add data and fetch data from.
const { Rider } = require("../../../model/index");

// Types
// HttpException : The Exception that needs to be thrown when some exception occurs in the code
const { HttpException } = require("../../../types/index");

// Handlers
// tokenHandler : This handler handles the token based functions.
// passwordHandler : This handler handles the password based functions.
// otpHandler : This handler handles the OTP based functions.
const { tokenHandler, passwordHandler, otpHandler } = require("../../../lib/index").shared;

const registerController = async (req, res, next) => {
	/**
	 * @apiDefine RegisterParams
	 * @apiParam {String} name Name of the Rider
	 * @apiParam {String} email Email of the Rider
	 * @apiParam {String} password Password of the Rider
	 * @apiParam {String} phone Phone Number of the Rider
	 * @apiParamExample {JSON} Request Body
	 * {
	 * 	name : John,
	 * 	email : "johndoe@example.com",
	 * 	phone : 9876543210,
	 * 	password : examplepassword
	 * }
	 */
	let { name, email, password, phone } = req.body;

	try {
		// Check if the rider exists by passing phone or email. If rider exists, throw HttpException
		if (await Rider.findOne({ email }).exec()) throw new HttpException("UserExists", `email already exists`);
		if (await Rider.findOne({ phone }).exec()) throw new HttpException("UserExists", `phone already exists`);

		// Convert the user entered password to hashed password.
		// More : https://en.wikipedia.org/wiki/Cryptographic_hash_function
		password = await passwordHandler.hashPassword(password);

		// Create a new Rider with given parameters.
		// NOTE : On creation, erros might be thrown by the Rider Model (More on src/model/rider/rider.model.js)
		let rider = await Rider.create({ name, email, phone, password });

		// Generate a new AccessToken with provided Params.
		let accessTokenParams = { _id: rider._id, type: "access" };
		let accessToken = await tokenHandler.generateAccessToken(accessTokenParams);

		// Generate a new RefreshToken with provided Params.
		let refreshTokenParams = { _id: rider._id, type: "refresh" };
		let refreshToken = await tokenHandler.generateRefreshToken(refreshTokenParams);

		// Generate a new OTP of 4 Digits
		let otp = otpHandler.generateOTP(4);

		// Populate the updatedRecord variable with newly generated values.
		// Then, use the updatedRecord to update the rider's document with new generated values.
		let updatedRecord = { accessToken, refreshToken, "otp.value": otp };
		rider = await Rider.findOneAndUpdate({ _id: rider._id }, updatedRecord, { new: true }).lean().exec();

		// TODO:Priority3 (Generate a better OTP ID with time quantum)
		let otpVerificationID = await tokenHandler.generateOTPToken({ _id: rider._id });

		/**
		 * @apiDefine RegisterSuccess
		 * @apiSuccess {String} otpVerificationID Generated OTPVerificationID which should be used while verifying the OTP. (More details in the OTP Verification section)
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	otpVerificationID : ID
		 * }
		 */
		res.data = { otpVerificationID };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		// NOTE : ValidationError is occured from the Database Model's side.
		if (err.name == "ValidationError") err = new HttpException("BadSyntaxField", err.message);
		next(err);
	}
};

module.exports = registerController;
