// Models
// Rider : This is the Rider's Model that is used to add data and fetch data from.
const { Rider } = require("../../../model/index");

// Types
// HttpException : The Exception that needs to be thrown when some exception occurs in the code
const { HttpException } = require("../../../types/index");

// Handlers
// tokenHandler : This handler handles the token based functions.
// passwordHandler : This handler handles the password based functions.
const { passwordHandler, tokenHandler, otpHandler } = require("../../../lib/index").shared;

// This function will take a rider model and refresh the tokens (ie. If invalid, replace with new one's and send them back)
async function refreshRiderTokens(rider) {
	return new Promise(async (resolve, reject) => {
		try {
			// Create variables to hold accessToken, refreshToken
			let accessToken = null;
			let refreshToken = null;
			let tokens = new Object();

			try {
				// Check if the token is invalid. If not invalid, populate accessToken and refreshToken variables.
				// If invalid, throw error to the catch(err) block.
				await tokenHandler.verifyJwt(rider.accessToken);
				await tokenHandler.verifyJwt(rider.refreshToken);
				accessToken = rider.accessToken;
				refreshToken = rider.refreshToken;
			} catch (err) {
				// Generate new AccessToken and RefreshToken and populate their variables.
				accessToken = await tokenHandler.generateAccessToken({ _id: rider._id, type: "access" });
				refreshToken = await tokenHandler.generateRefreshToken({ _id: rider._id, type: "refresh" });
			}

			// Populate the updatedRecord with populated token variables.
			// Then use the updatedRecord to update the rider's document with new generated values.
			tokens["accessToken"] = accessToken;
			tokens["refreshToken"] = refreshToken;
			rider = await Rider.findOneAndUpdate({ _id: rider._id }, tokens, { new: true }).lean().exec();
			resolve(tokens);
		} catch (err) {
			reject(err);
		}
	});
}

const loginController = async (req, res, next) => {
	/**
	 * @apiDefine LoginParams
	 * @apiParam {String} email Email of the Rider
	 * @apiParam {String} phone Phone Number of the Rider
	 * @apiParam {String} password Password of the Rider
	 * @apiParamExample  {JSON} Request Body
	 * {
	 * 	email : "johndoe@example.com", (or phone number like, phone : 9876543210)
	 * 	password : examplepassword
	 * }
	 */
	let { email, phone, password } = req.body;

	try {
		// Decide if the email or phone is provided for login.
		// If both are not provided, throw an HttpException
		// TODO:Priority3 (Convert below three lines into one line)
		let data = new Object();
		if (email) data["email"] = email;
		else if (phone) data["phone"] = phone;
		else throw new HttpException("FieldRequired", "email or phone number is required");

		// Check if the password is provided by the user. If not, throw error.
		if (!password) throw new HttpException("FieldRequired", "password is required");

		// Check if the rider exists by passing the populated 'data'
		// If the rider doesn't exists, throw error
		let rider = await Rider.findOne(data).exec();
		if (!rider) throw new HttpException("UserNotExists", "rider doesn't exists");

		// Check if the provided password matches the hashed password that is stored on the rider's document.
		// If not, throw error.
		let isValidPassword = await passwordHandler.isMatched(password, rider.password);
		if (!isValidPassword) throw new HttpException("PasswordMismatch", "password is invalid");

		let responseData = null;
		if (!rider.otp.isVerified) {
			// If the otp is not verified, generate an OTP and send otpverification id
			let otp = otpHandler.generateOTP(4);
			rider = await Rider.findOneAndUpdate({ _id: rider._id }, { "otp.value": otp }, { new: true }).lean().exec();

			// TODO:Priority3 (Generate a better OTP ID with time quantum)
			let otpVerificationID = await tokenHandler.generateOTPToken({ _id: rider._id });

			responseData = { otpVerificationID };
		} else {
			// If the otp is verified, send access token and refresh token
			let tokens = await refreshRiderTokens(rider);
			responseData = { accessToken: tokens.accessToken, refreshToken: tokens.refreshToken };
		}

		/**
		 * @apiDefine LoginSuccess
		 * @apiSuccess {String} accessToken Access token generated for the specific user which will be used to request resources from the server.
		 * @apiSuccess {String} refreshToken Refresh token generated for the specific user which will be used to refresh the accessToken.
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	accessToken : AccessToken
		 * 	refreshToken : RefreshToken
		 * }
		 */
		res.data = responseData;
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		next(err);
	}
};

module.exports = loginController;
