// Types
// HttpException : The Exception that needs to be thrown when some exception occurs in the code
const { HttpException } = require("../../../types");

// Handlers
// otpHandler : This handler handles the OTP based functions.
const { otpHandler } = require("../../../lib/index").shared;

const verifyOTP = async (req, res, next) => {
	/**
	 * @apiDefine VerifyOtpParams
	 * @apiParam {String} otpVerificationID This ID is used to identify a OTP Authentication session. In this section, this ID will reset the OTP Session and generate a new OTP.
	 * @apiParam {String} otp This is the OTP that is received in the client's end
	 * @apiParamExample {JSON} Request Body
	 * data : {
	 * 	otpVerificationID : ID
	 * 	otp : OTP
	 * }
	 */
	let { otpVerificationID, otp } = req.body;

	try {
		// Pass the otpVerificationID and the OTP to otpHandler
		// otpHandler either provides accessToken and refreshToken, if the params are valid
		// or it throws error, if any errors occur. Which will be passed to catch block.
		let { accessToken, refreshToken } = await otpHandler.verifyOTP(otpVerificationID, otp);

		/**
		 * @apiDefine VerifyOtpSuccess
		 * @apiSuccess {String} accessToken Access token generated for the specific user which will be used to request resources from the server.
		 * @apiSuccess {String} refreshToken Refresh token generated for the specific user which will be used to refresh the accessToken.
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	accessToken : AccessToken
		 * 	refreshToken : RefreshToken
		 * }
		 */
		res.data = { accessToken, refreshToken };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		next(err);
	}
};

module.exports = verifyOTP;
