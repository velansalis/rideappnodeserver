// Models
// Rider : This is the Rider's Model that is used to add data and fetch data from.
const { Rider } = require("../../../model/index");

// Types
// HttpException : The Exception that needs to be thrown when some exception occurs in the code
const { HttpException } = require("../../../types/index");

// Handlers
// tokenHandler : This handler handles the token based functions.
const { tokenHandler } = require("../../../lib/index").shared;

const tokenRefresh = async (req, res, next) => {
	/**
	 * @apiDefine RefreshTokenParams
	 * @apiParam {String} refreshToken The RefreshToken of the specific user
	 * @apiParamExample  {JSON} Request Body
	 * {
	 * 	refreshToken : Token
	 * }
	 */
	let { refreshToken } = req.body;
	let accessToken = null;

	try {
		// Decode the refreshToken received by the user to obtain user id.
		let decoded = await tokenHandler.verifyJwt(refreshToken);

		// Get Rider's document through the user id. If the rider doesn't exists,
		// throw HttpException of UserNotExists.
		let rider = await Rider.findOne({ _id: decoded._id }).lean().exec();
		if (!rider) throw new HttpException("UserNotExists", "rider doesn't exists");

		// Check if the refresh token is invalid. If the token is invalid, throw HttpException.
		if (decoded.type != "refresh" || refreshToken != rider.refreshToken) throw new HttpException("UnauthorizedAccess", "invalid refresh	token");

		// Generate new AccessToken and RefreshToken. Update rider's document with new AccessToken and RefreshToken.
		accessToken = await tokenHandler.generateAccessToken({ _id: rider._id, type: "access" });
		refreshToken = await tokenHandler.generateRefreshToken({ _id: rider._id, type: "refresh" });
		rider = await Rider.findOneAndUpdate({ _id: rider._id }, { accessToken, refreshToken }, { new: true }).lean().exec();

		/**
		 * @apiDefine RefreshTokenSuccess
		 * @apiSuccess {String} accessToken Access token generated for the specific user which will be used to refresh bearer token.
		 * @apiSuccess {String} bearerToken Bearer token generated for the specific user which will be used to make further requests to the server.
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	accessToken : AccessToken
		 * 	RefreshToken : RefreshToken
		 * }
		 */
		res.data = { accessToken, refreshToken };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		next(err);
	}
};

module.exports = tokenRefresh;
