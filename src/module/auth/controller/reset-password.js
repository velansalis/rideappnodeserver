// Models
// Rider : This is the Rider's Model that is used to add data and fetch data from.
const { Rider } = require("../../../model/index");

// Types
// HttpException : The Exception that needs to be thrown when some exception occurs in the code
const { HttpException } = require("../../../types/index");

// Handlers
// passwordHandler : This handler handles the password based functions.
const { passwordHandler } = require("../../../lib/index").shared;

const resetPasswordController = async (req, res, next) => {
	/**
	 * @apiDefine ResetPasswordParams
	 * @apiParam {String} newPassword New password of the rider
	 * @apiParamExample {JSON} Request Body
	 * {
	 * 	newPassword : ExamplePassword
	 * }
	 */
	let { newPassword } = req.body;
	let { _id } = req.token;

	try {
		// Convert the user entered password to hashed password.
		// More : https://en.wikipedia.org/wiki/Cryptographic_hash_function
		newPassword = await passwordHandler.hashPassword(newPassword);

		// Change the user's current password with new hashed password.
		let rider = await Rider.findOneAndUpdate({ _id }, { password: newPassword }, { new: true, runValidators: true }).lean().exec();

		// Update message that needs to be sent back to the user.
		let message = "password successfully reset";

		/**
		 * @apiDefine ResetPasswordSuccess
		 * @apiSuccess {String} message Success message on completion of the request
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	message : "password successfully reset"
		 * }
		 */
		res.data = { message };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		next(err);
	}
};

module.exports = resetPasswordController;
