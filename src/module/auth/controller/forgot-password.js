// Models
// Rider : This is the Rider's Model that is used to add data and fetch data from.
const { Rider } = require("../../../model/index");

// Types
// HttpException : The Exception that needs to be thrown when some exception occurs in the code
const { HttpException } = require("../../../types/index");

// Handlers
// tokenHandler : This handler handles the token based functions.
// otpHandler : This handler handles the OTP based functions.
const { tokenHandler, otpHandler } = require("../../../lib/index").shared;

const forgotPasswordController = async (req, res, next) => {
	/**
	 * @apiDefine ForgotPasswordParams
	 * @apiParam {String} email Email of the Rider
	 * @apiParam {String} phone Phone Number of the Rider
	 * @apiParamExample {JSON} Request Body
	 * { email : "johndoe@example.com" }
	 * { phone : 9876543210 }
	 */
	const { email, phone } = req.body;

	try {
		// Decide if the email or phone is provided for login.
		// If both are not provided, throw an HttpException
		// TODO:Priority3 (Convert below three lines into one line)
		let data = new Object();
		if (email) data["email"] = email;
		else if (phone) data["phone"] = phone;
		else throw new HttpException("FieldRequired", "email or phone number is required");

		// Check if the rider exists by passing the populated 'data'
		// If the rider doesn't exists, throw error
		let rider = await Rider.findOne(data).exec();
		if (!rider) throw new HttpException("UserNotExists", "rider doesn't exists");

		// Create variables to hold accessToken, refreshToken
		// and updateRecord (which will be used when the field has to be updated)
		let accessToken = null;
		let refreshToken = null;
		let updateRecord = new Object();

		try {
			// Check if the token is invalid. If invalid, the error will be thrown to
			// the catch(err) block.
			await tokenHandler.verifyJwt(rider.accessToken);
			await tokenHandler.verifyJwt(rider.refreshToken);
		} catch (err) {
			// Generate new AccessToken and RefreshToken and store it in their variables.
			// Update the updateRecord and prepare it to be updated.
			accessToken = await tokenHandler.generateAccessToken({ _id: rider._id, type: "access" });
			refreshToken = await tokenHandler.generateRefreshToken({ _id: rider._id, type: "refresh" });
			updateRecord["accessToken"] = accessToken;
			updateRecord["refreshToken"] = refreshToken;
		}

		// Generate the OTP of 4 digits from otpHandler (default - 1234) and
		// add the otp to the updateRecord. Update the rider's document with OTP, new AccessToken and RefreshToken.
		let otp = otpHandler.generateOTP(4);
		updateRecord["otp.value"] = otp;
		rider = await Rider.findOneAndUpdate({ _id: rider._id }, updateRecord, { new: true }).lean().exec();

		// TODO:Priority3 (Generate a better OTP ID with time quantum)
		let otpVerificationID = await tokenHandler.generateOTPToken({ _id: rider._id });

		/**
		 * @apiDefine ForgotPasswordSuccess
		 * @apiSuccess {String} otpVerificationID Generated OTPVerificationID which should be used while verifying the OTP. (More details in the OTP Verification section)
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	otpVerificationID : ID
		 * }
		 */
		res.data = { otpVerificationID };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/uril/response.handler.js)
		next(err);
	}
};

module.exports = forgotPasswordController;
