// Models
// Rider : This is the Rider's Model that is used to add data and fetch data from.
const { Rider } = require("../../../model/index");

// Types
// HttpException : The Exception that needs to be thrown when some exception occurs in the code
const { HttpException } = require("../../../types/index");

// Handlers
// passwordHandler : This handler handles the password based functions.
const { passwordHandler } = require("../../../lib/index").shared;

const terminateController = async (req, res, next) => {
	/**
	 * @apiDefine TerminateParams
	 * @apiParam {String} email Email of the Rider
	 * @apiParam {String} phone Phone Number of the Rider
	 * @apiParam {String} password Password of the Rider
	 * @apiParamExample  {JSON} Request Body
	 * {
	 * 	email : "johndoe@example.com", (or phone number like, phone : 9876543210)
	 * 	password : examplepassword
	 * }
	 */
	let { email, phone, password } = req.body;

	try {
		// Decide if the email or phone is provided for login.
		// If both are not provided, throw an HttpException
		// TODO:Priority3 (Convert below three lines into one line)
		let data = new Object();
		if (email) data["email"] = email;
		else if (phone) data["phone"] = phone;
		else throw new HttpException("FieldRequired", "email or phone number is required");

		// Check if the password is provided by the user. If not, throw error.
		if (!password) throw new HttpException("FieldRequired", "password is required");

		// Check if the rider exists by passing the populated 'data'
		// If the rider doesn't exists, throw error
		let rider = await Rider.findOne(data).exec();
		if (!rider) throw new HttpException("UserNotExists", "rider doesn't exists");

		// Check if the provided password matches the hashed password that is stored on the rider's document.
		// If not, throw error.
		let isValidPassword = await passwordHandler.isMatched(password, rider.password);
		if (!isValidPassword) throw new HttpException("PasswordMismatch", "password is invalid");

		// Delete the rider's account.
		// NOTE (To future self) : You can also make this unreadable instead of deleting by which you can undo deletion.
		rider = await Rider.findOneAndDelete(data).lean().exec();

		// Update message that needs to be sent back to the user.
		let message = "rider successfully deleted";

		/**
		 * @apiDefine TerminateSuccess
		 * @apiSuccess {String} message Success message
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	message : "rider successfully deleted"
		 * }
		 */
		res.data = { message };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		next(err);
	}
};

module.exports = terminateController;
