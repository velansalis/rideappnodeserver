// Models
// Rider : This is the Rider's Model that is used to add data and fetch data from.
const { Rider } = require("../../../model/index");

// Types
// HttpException : The Exception that needs to be thrown when some exception occurs in the code
const { HttpException } = require("../../../types/index");

// Handlers
// otpHandler : This handler handles the OTP based functions.
const { otpHandler, tokenHandler } = require("../../../lib/index").shared;

const resetOTP = async (req, res, next) => {
	/**
	 * @apiDefine ResetOtpParams
	 * @apiParam {String} otpVerificationID This ID is used to identify a OTP Authentication session. In this section, this ID will reset the OTP Session and generate a new OTP.
	 * @apiParamExample {JSON} Request Body
	 * data : {
	 * 	otpVerificationID : ID
	 * }
	 */
	let { otpVerificationID } = req.body;

	try {
		let decodedToken = await tokenHandler.verifyJwt(otpVerificationID);

		// Generate the OTP of 4 digits from otpHandler (default - 1234)
		// add the otp to the otpRecord. Update the rider's document with OTP.
		let otp = otpHandler.generateOTP(4);
		let otpRecord = { "otp.value": otp };

		// Update the rider by passing the populated _id from otpVerificationID
		// If the rider doesn't exists, throw error
		let rider = await Rider.findOneAndUpdate({ _id: decodedToken._id }, otpRecord, { new: true }).lean().exec();
		if (!rider) throw new HttpException("UserNotExists", "rider doesn't exists");

		// TODO:Priority3 (Generate a better OTP ID with time quantum)
		otpVerificationID = await tokenHandler.generateOTPToken({ _id: rider._id });

		/**
		 * @apiDefine ResetOtpSuccess
		 * @apiSuccess {String} otpVerificationID Newly generated OTP Verification ID
		 * @apiSuccessExample {JSON} Success Response:
		 * data : {
		 * 	otpVerificationID : ID
		 * }
		 */
		res.data = { otpVerificationID };
		next();
	} catch (err) {
		// Any errors occuring in the body of the request is caught here and passed to
		// Error handling middleware. (src/lib/util/response.handler.js)
		next(err);
	}
};

module.exports = resetOTP;
