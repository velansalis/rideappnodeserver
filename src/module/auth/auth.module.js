module.exports = {
	/**
	 * @api {post} {BASE_URL}/api/auth/login Login Rider
	 * @apiName Login
	 * @apiDescription This module authenticates and logs the user in. Returns the bearer token. (You can either use phone number and password or
	 * you can use email id and password for login)
	 * @apiGroup Authentication
	 * @apiVersion 1.0.0
	 * @apiPermission None
	 * @apiUse LoginParams
	 * @apiUse LoginSuccess
	 * @apiUse ErrorResponse
	 */
	loginController: require("./controller/login"),

	/**
	 * @api {post} {BASE_URL}/api/auth/register Register a Rider
	 * @apiName Register
	 * @apiDescription This module registers the user and generates a bearer token. Returns the bearer token.
	 * @apiGroup Authentication
	 * @apiVersion  1.0.0
	 * @apiPermission None
	 * @apiUse RegisterParams
	 * @apiUse RegisterSuccess
	 * @apiUse ErrorResponse
	 */
	registerController: require("./controller/register"),

	/**
	 * @api {post} {BASE_URL}/api/auth/terminate Terminate Account
	 * @apiName Terminate Account
	 * @apiDescription This module autheticates the user and deletes his/her account.
	 * @apiGroup Authentication
	 * @apiVersion 1.0.0
	 * @apiPermission None
	 * @apiUse TerminateParams
	 * @apiUse TerminateSuccess
	 * @apiUse ErrorResponse
	 */
	terminateController: require("./controller/terminate"),

	/**
	 * @api {post} {BASE_URL}/api/auth/forgot-password Forgot Password
	 * @apiName Forgot Password
	 * @apiDescription This module sends a reset link to user's provided email id or phone number. (You can either pass registered email id or phone number)
	 * @apiGroup Authentication
	 * @apiVersion 1.0.0
	 * @apiPermission None
	 * @apiUse ForgotPasswordParams
	 * @apiUse ForgotPasswordSuccess
	 * @apiUse ErrorResponse
	 */
	forgotPasswordController: require("./controller/forgot-password"),

	/**
	 * @api {post} {BASE_URL}/api/auth/reset-password Reset Password
	 * @apiName Reset Password
	 * @apiDescription This module replaces the user's old password with the provided new password.
	 * @apiGroup Authentication
	 * @apiVersion 1.0.0
	 * @apiPermission Rider
	 * @apiUse BearerToken
	 * @apiUse ResetPasswordParams
	 * @apiUse ResetPasswordSuccess
	 * @apiUse ErrorResponse
	 */
	resetPasswordController: require("./controller/reset-password"),

	/**
	 * @api {post} {BASE_URL}/api/auth/otp/verify Verify OTP
	 * @apiName Verify OTP
	 * @apiDescription This module verifies the OTP of the user and sends the bearer token back.
	 * @apiGroup Authentication
	 * @apiVersion 1.0.1
	 * @apiPermission Rider
	 * @apiUse VerifyOtpParams
	 * @apiUse VerifyOtpSuccess
	 * @apiUse ErrorResponse
	 */
	verifyOTP: require("./controller/verify-otp"),

	/**
	 * @api {post} {BASE_URL}/api/auth/otp/reset Reset OTP
	 * @apiName Reset OTP
	 * @apiDescription This module replaces the user's old OTP with a new one and resets the clock.
	 * @apiGroup Authentication
	 * @apiVersion 1.0.1
	 * @apiPermission Rider
	 * @apiUse ResetOtpParams
	 * @apiUse ResetOtpSuccess
	 * @apiUse ErrorResponse
	 */
	resetOTP: require("./controller/reset-otp"),

	/**
	 * @api {post} {BASE_URL}/api/auth/oauth/refresh Refresh Tokens
	 * @apiName Refresh Tokens
	 * @apiDescription This module refreshes the Access token and Bearer token of the user
	 * @apiGroup Authentication
	 * @apiVersion 1.0.2
	 * @apiPermission Rider
	 * @apiUse RefreshTokenParams
	 * @apiUse RefreshTokenSuccess
	 * @apiUse ErrorResponse
	 */
	refreshToken: require("./controller/token-refresh"),
};
