const onError = (socket) => {
	socket.on("error", (error) => {
		console.log(error);
	});
};

module.exports = onError;
