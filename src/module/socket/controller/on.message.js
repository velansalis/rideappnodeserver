const onMessage = (socket) => {
	socket.on("message", (data) => {
		console.log("message received", data);
	});
};

module.exports = onMessage;
