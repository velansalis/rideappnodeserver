const onDisconnection = (socket) => {
	socket.on("disconnect", async () => {
		console.log(`Socket ${socket.id} disconnected`, socket.decodedToken);
		try {
			rider = await Rider.findOneAndUpdate({ _id: socket.decodedToken._id }, { socket: "", status: "offline" }, { new: true }).lean().exec();
		} catch (err) {
			socket.emit("connect_error", { message: "invalid rider id" });
		}
	});
};

module.exports = onDisconnection;
