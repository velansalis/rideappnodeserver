const { Rider } = require("../../model/index");
const tokenHandler = require("../../lib/shared/logging.handler");

const onMessage = require("./controller/on.message");
const onError = require("./controller/on.error");
const onDisconnection = require("./controller/on.disconnect");

const socketConnected = async (socket) => {
	console.log(`Socket ${socket.id} Connected.`);

	let token = socket.handshake.query.token;
	let rider = null;

	try {
		// Verify Decoded Token
		socket.decodedToken = await tokenHandler.verifyJwt(token);

		// Update user parameter
		let query = { _id: socket.decodedToken._id };
		let updateQuery = { socket: socket.id, status: "online" };
		let options = { new: true };
		rider = await Rider.findOneAndUpdate(query, updateQuery, options).lean().exec();
	} catch (err) {
		socket.emit("connect_error", { message: err.message });
		socket.disconnect();
	}

	onMessage(socket);
	onError(socket);
	onDisconnection(socket);
};

module.exports = (server) => {
	require("socket.io")(server).on("connection", socketConnected);
};
