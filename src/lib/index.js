module.exports = {
	shared: {
		fileUploadHandler: require("./shared/file-upload.handler"),
		loggingHandler: require("./shared/logging.handler"),
		passwordHandler: require("./shared/password.handler"),
		timestampHandler: require("./shared/timestamp.handler"),
		tokenHandler: require("./shared/token.handler"),
		otpHandler: require("./shared/otp.handler"),
	},
	util: {
		environmentHandler: require("./util/environment.handler"),
		mongooseHandler: require("./util/mongoose.handler"),
		responseHandler: require("./util/response.handler"),
		serverHandler: require("./util/server.handler"),
	},
};
