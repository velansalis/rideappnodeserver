const chalk = require("chalk");
const timestamp = require("./timestamp.handler");

const interceptor = (option) => {
	return (req, res, next) => {
		if (process.env.NODE_ENV == "test") return next();
		let url = req.url;
		let method = req.method;
		let version = req.httpVersion;
		let startTime = new Date();
		next();
		let endTime = new Date();
		let timeElapsed = timestamp.differenceInMs(endTime, startTime);
		console.log("\n", chalk.green(`[${method}]`), chalk.bold.whiteBright(url), chalk.green(version), chalk.green(timeElapsed + "ms"));
	};
};

const logInfo = (level, options) => {
	let title = level == "warn" ? chalk.yellow(`[${options.title}]`) : level == "info" ? chalk.blue(`[${options.title}]`) : chalk.green(`[${options.title}]`);
	console.log(title, chalk.bold.white(`${options.message}`));
};

module.exports = {
	interceptor,
	logInfo,
};
