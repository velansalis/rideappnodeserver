const bcrypt = require("bcryptjs");

const { HttpException } = require("../../types/index");

// TODO:Priority3 (Isolate the validation to the separate function)
const hashPassword = (password) => {
	return new Promise((resolve, reject) => {
		try {
			if (!password) throw new HttpException("FieldRequired", "password is required");
			if (password.length < 8) throw new HttpException("BadSyntaxField", "password should be more than 8 characters");
			let hashed = bcrypt.hash(password, 10);
			resolve(hashed);
		} catch (err) {
			reject(err);
		}
	});
};

const isMatched = (password, hashedPassword) => {
	return new Promise((resolve, reject) => {
		bcrypt
			.compare(password, hashedPassword)
			.then((data) => {
				resolve(data);
			})
			.catch((err) => {
				reject(err);
			});
	});
};

module.exports = {
	hashPassword,
	isMatched,
};
