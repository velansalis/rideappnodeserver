const moment = require("moment");

const { HttpException } = require("../../types/index");

const formatDate = (date) => {
	return new Promise((resolve, reject) => {
		try {
			if (!date) throw new HttpException("BadSyntaxField", `invalid date/time format`);
			let formattedDate = moment(`${date}`, "DD/MM/YYYY");
			resolve(formattedDate);
		} catch (err) {
			reject(err);
		}
	});
};

const formatDateAndTime = (date, time) => {
	return new Promise((resolve, reject) => {
		try {
			if (!date || !time) throw new HttpException("BadSyntaxField", `invalid date/time format`);
			let formattedDate = moment(`${date} ${time}`, "DD/MM/YYYY hh:mm");
			resolve(formattedDate);
		} catch (err) {
			reject(err);
		}
	});
};

const isGreaterThan = (date1, date2) => {
	return new Promise((resolve, reject) => {
		date1 = moment(date1);
		date2 = moment(date2);
		if (date1 > date2) resolve(true);
		else resolve(false);
	});
};

const differenceInMs = (dateOne, dateTwo) => {
	dateOne = moment(dateOne);
	dateTwo = moment(dateTwo);
	var duration = moment.duration(dateOne.diff(dateTwo));
	return duration.asMilliseconds();
};

const differenceInSec = (dateOne, dateTwo) => {
	dateOne = moment(dateOne);
	dateTwo = moment(dateTwo);
	var duration = moment.duration(dateOne.diff(dateTwo));
	return duration.asSeconds();
};

module.exports = {
	formatDate,
	formatDateAndTime,
	differenceInMs,
	differenceInSec,
	isGreaterThan,
};
