const { Rider } = require("../../model/index");

const { HttpException } = require("../../types/index");

const tokenHandler = require("./token.handler");

let generateOTP = (numberOfDigits) => {
	let OTP = "";
	for (let i = 0; i < numberOfDigits; i++) {
		// OTP += Math.floor(Math.random() * 10);
		OTP += i + 1;
	}
	return OTP;
};

let verifyOTP = (otpVerificationID, otp) => {
	return new Promise(async (resolve, reject) => {
		let OTPExpiry = 30;

		try {
			let decodedToken = await tokenHandler.verifyJwt(otpVerificationID);

			let iat = decodedToken.iat;
			let now = Math.floor(Date.now() / 1000);
			if (now >= iat + OTPExpiry) throw new HttpException("OTPExpired");

			let rider = await Rider.findOne({ _id: decodedToken._id }).lean().exec();
			if (!rider) throw new HttpException("UserNotExists", "rider doesn't exists");

			if (otp != rider.otp.value) throw new HttpException("OTPInvalid");

			rider = await Rider.findOneAndUpdate({ _id: rider._id }, { "otp.isVerified": true }, { new: true }).lean().exec();
			resolve({ accessToken: rider.accessToken, refreshToken: rider.refreshToken });
		} catch (err) {
			reject(err);
		}
	});
};

module.exports = {
	generateOTP,
	verifyOTP,
};
