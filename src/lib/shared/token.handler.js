const jwt = require("jsonwebtoken");

const verifyJwt = (token) => {
	return new Promise((resolve, reject) => {
		jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {
			if (err) reject(err);
			else resolve(decoded);
		});
	});
};

const signJwt = (data, expiresIn) => {
	return new Promise((resolve, reject) => {
		let token = jwt.sign(data, process.env.TOKEN_SECRET, {
			expiresIn,
		});
		resolve(token);
	});
};

/**
 * @apiDefine BearerToken
 *
 * @apiHeader {String} token AccessToken returned during login or register.
 * @apiHeaderExample {JSON} Header Example
 * {
 * 	Authorization : "Bearer token"
 * }
 */
const generateAccessToken = async (data) => {
	let accessTokenExpiry = "1d";
	let accessToken = await signJwt(data, accessTokenExpiry);
	return accessToken;
};

const generateRefreshToken = async (data) => {
	let refreshTokenExpiry = "30d";
	let refreshToken = await signJwt(data, refreshTokenExpiry);
	return refreshToken;
};

const generateOTPToken = async (data) => {
	let otpTokenExpiry = "1h";
	let otpToken = await signJwt(data, otpTokenExpiry);
	return otpToken;
};

module.exports = {
	verifyJwt,
	signJwt,
	generateRefreshToken,
	generateAccessToken,
	generateOTPToken,
};
