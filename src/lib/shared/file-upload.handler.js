const multer = require("multer");
const path = require("path");

var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, `uploads/`);
	},
	filename: function (req, file, cb) {
		cb(null, `avatar-${Date.now()}${path.extname(file.originalname)}`);
	},
});

const avatarUploadHandler = (req, res, next) => {
	let upload = multer({ storage }).single("avatar");
	upload(req, res, (err) => {
		if (err instanceof multer.MulterError) {
			next(err);
		} else if (err) {
			next(err);
		}
		next();
	});
};

module.exports = {
	avatarUploadHandler,
};
