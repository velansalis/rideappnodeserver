const http = require("http");
const app = require("../../app");
const server = http.Server(app);
const io = require("../../module/socket/socket.module")(server);

const logger = require("../shared/logging.handler");

const startServer = async (PORT) => {
	return new Promise((resolve, reject) => {
		server.listen(PORT, () => {
			logger.logInfo("info", { title: `Server`, message: `Connected on port ${PORT}` });
			resolve();
		});
	});
};

const closeServer = () => {
	logger.logInfo("info", { title: `Server`, message: `Gracefully stopped the server` });
	logger.logInfo("info", { title: `Server`, message: `Server closed` });
};

module.exports = {
	startServer,
	closeServer,
};
