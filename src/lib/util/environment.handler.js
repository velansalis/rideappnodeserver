const dotenv = require("dotenv");
const path = require("path");

const logger = require("../shared/logging.handler");

const envFiles = {
	test: path.resolve(__dirname, "../../../env/test.env"),
	dev: path.resolve(__dirname, "../../../env/dev.env"),
};

const loadEnvironmentVariables = () => {
	return new Promise((resolve, reject) => {
		const env = process.env.NODE_ENV;
		dotenv.config({ path: envFiles[env] });
		logger.logInfo("info", { title: `Environment`, message: `Loaded environment (${env})` });
		resolve();
	});
};

module.exports = {
	loadEnvironmentVariables,
};
