const mongoose = require("mongoose");
const logger = require("../shared/logging.handler");

mongoose.connection
	.on("open", () => {
		if (process.env.NODE_ENV == "test") {
			logger.logInfo("warn", { title: `Database`, message: `Cleared the database for test` });
			mongoose.connection.db.dropDatabase();
		}
		logger.logInfo("info", { title: `Database`, message: `Connected on port 27017.` });
	})
	.on("error", (err) => {
		console.log(err);
	});

const connectMongoose = (options) => {
	return new Promise((resolve, reject) => {
		mongoose
			.connect(`${process.env.DB_ADDRESS}`, {
				useCreateIndex: true,
				useNewUrlParser: true,
				useUnifiedTopology: true,
				useFindAndModify: false,
			})
			.then(() => resolve())
			.catch((err) => reject(err));
	});
};

const disconnectMongoose = () => {
	logger.logInfo("info", { title: `Database`, message: `Disconnected` });
	mongoose.disconnect();
};

module.exports = {
	connectMongoose,
	disconnectMongoose,
};
