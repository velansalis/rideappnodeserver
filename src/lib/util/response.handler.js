const success = (req, res, next) => {
	const { data } = res;
	res.status(200);
	res.json({ data });
};

/**
 * @apiDefine ErrorResponse
 * @apiError {String} name Appropriate error name
 * @apiError {String} message Appropriate error message
 * @apiErrorExample {JSON} Error Response
 * {
 * 	name : ErrorName
 * 	message : ErrorMessage
 * }
 */
const error = (err, req, res, next) => {
	let { type, name, message, status } = err;
	let responseObject = new Object();

	if (type && status) {
		res.status(status);
		responseObject.name = name;
		responseObject.message = message;
	} else {
		responseObject = { message };
	}
	res.json(responseObject);
};

module.exports = {
	success,
	error,
};
