class CustomException extends Error {
	constructor(type, name) {
		super();
		this.type = type;
		this.name = name;
	}
}

class HttpException extends CustomException {
	constructor(name, message) {
		super("HttpException", name);
		this.message = this.errorRef[name].defaultMessage || message;
		this.status = this.errorRef[name].status;
	}
}

HttpException.prototype.errorRef = {
	OTPSessionExpired: { status: 403, defaultMessage: "OTP session has expired" },
	TokenExpiredError: { status: 403, defaultMessage: "token has expired" },
	JsonWebTokenError: { status: 403, defaultMessage: "token is invalid" },
	OTPInvalid: { status: 403, defaultMessage: "otp is invalid" },
	OTPExpired: { status: 403, defaultMessage: "otp has expired" },
	FieldRequired: { status: 403 },
	BadSyntaxField: { status: 403 },
	PasswordMismatch: { status: 403 },
	UserExists: { status: 403 },
	UserNotExists: { status: 403 },
	Forbidden: { status: 403 },
	UnauthorizedAccess: { status: 403 },
	NotFound: { status: 404 },
};

module.exports = HttpException;
