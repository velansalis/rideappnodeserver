const mongoose = require("mongoose");

const ValidationError = require("mongoose-validation-error-transform");
const UniqueValidation = require("mongoose-beautiful-unique-validation");

const Schema = mongoose.Schema;

const ServiceSchema = new Schema({
	phone: { type: String, required: [true, "phone number is required"] },
	vehicleNumber: { type: String, required: [true, "vehicle number is required"] },
	vehicleType: { type: String, required: [true, "vehicle type is required"] },
	serviceType: { type: String, required: [true, "service type is required"] },
	slot: { type: Date, required: [true, "slot date is required"] },
	dealer: { type: mongoose.Types.ObjectId, ref: "Dealer", required: [true, "dealer is required"] },
	comments: { type: String, default: "" },
	owner: { type: mongoose.Types.ObjectId, ref: "Rider" },
});

ServiceSchema.plugin(ValidationError, {
	capitalize: false,
	transform: function (messages) {
		return messages[0].toLowerCase();
	},
});

ServiceSchema.plugin(UniqueValidation);

module.exports = mongoose.model("Service", ServiceSchema);
