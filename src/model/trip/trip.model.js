const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ValidationError = require("mongoose-validation-error-transform");
const UniqueValidation = require("mongoose-beautiful-unique-validation");

const TripSchema = new Schema(
	{
		name: {
			type: String,
			required: [true, "trip name is required"],
			validate: [
				(value) => {
					if (value) return /^[A-Za-z ]+$/.test(value);
					else value;
				},
				"trip name should contain only letters",
			],
		},

		destination: {
			type: [Number],
			required: [true, "trip destination is required"],
			validate: [
				(value) => {
					return value.length == 2;
				},
				"invalid destination",
			],
		},

		source: {
			type: [Number],
			required: [true, "trip source is required"],
			validate: [
				(value) => {
					return value.length == 2;
				},
				"invalid source",
			],
		},

		startDate: { type: Date, required: [true, "trip start date is required"], validate: () => {} },

		endDate: { type: Date, required: [true, "trip end date is required"] },

		milestones: {
			type: [[Number]],
			validate: [
				(milestones) => {
					for (let milestone of milestones) {
						if (milestone.length != 2) return false;
					}
					return true;
				},
				`invalid milestone`,
			],
		},

		invitedUsers: {
			type: [Schema.Types.ObjectId],
		},

		owner: { type: String, required: true, ref: "Rider" },
	},
	{
		timestamps: true,
		strict: true,
	}
);

TripSchema.statics.isValid = (id) => {
	return mongoose.Types.ObjectId.isValid(id);
};

TripSchema.plugin(ValidationError, {
	capitalize: false,
	transform: function (messages) {
		return messages[0].toLowerCase();
	},
});

TripSchema.plugin(UniqueValidation);

module.exports = mongoose.model("Trip", TripSchema);
