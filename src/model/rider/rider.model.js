const mongoose = require("mongoose");

const ValidationError = require("mongoose-validation-error-transform");
const UniqueValidation = require("mongoose-beautiful-unique-validation");

const Schema = mongoose.Schema;

const RiderSchema = new Schema(
	{
		name: {
			type: String,
			required: [true, "name is required"],
			validate: [
				(value) => {
					if (value) return /^[A-Za-z ]+$/.test(value);
					else value;
				},
				"name can contain only letters",
			],
			set: (value) => {
				if (value)
					return value
						.trim()
						.split(" ")
						.map((w) => w[0].toUpperCase() + w.substr(1).toLowerCase())
						.join(" ");
			},
		},
		email: {
			type: String,
			trim: true,
			lowercase: true,
			unique: true,
			required: [true, "email is required"],
			validate: [
				(email) => {
					let regxp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
					return regxp.test(email);
				},
				"invalid email address",
			],
		},
		phone: {
			type: String,
			trim: true,
			required: [true, "phone number is required"],
			validate: [
				(phone) => {
					let regxp = /^\d{10}$/;
					return regxp.test(phone);
				},
				"invalid phone number",
			],
		},
		password: {
			required: [true, "password"],
			type: String,
			trim: true,
			minlength: 8,
		},
		avatar: {
			type: String,
			default: null,
		},
		joinedTrips: [
			{
				type: mongoose.Types.ObjectId,
				ref: "Trip",
			},
		],
		services: [
			{
				type: mongoose.Types.ObjectId,
				ref: "Service",
			},
		],
		accessToken: {
			type: String,
		},
		refreshToken: {
			type: String,
		},
		otp: {
			value: { type: String },
			isVerified: { type: Boolean, default: false },
		},
		socket: {
			type: String,
		},
		status: {
			type: String,
			default: "offline",
			enum: ["online", "offline"],
		},
	},
	{
		timestamps: true,
	}
);

RiderSchema.plugin(ValidationError, {
	capitalize: false,
	transform: function (messages) {
		return messages[0].toLowerCase();
	},
});

RiderSchema.plugin(UniqueValidation);

module.exports = mongoose.model("Rider", RiderSchema);
