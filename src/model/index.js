module.exports = {
	Rider: require("./rider/rider.model"),
	Dealer: require("./dealer/dealer.model"),
	Service: require("./service/service.model"),
	Trip: require("./trip/trip.model"),
};
