const mongoose = require("mongoose");

const ValidationError = require("mongoose-validation-error-transform");
const UniqueValidation = require("mongoose-beautiful-unique-validation");

const Schema = mongoose.Schema;

const DealerSchema = new Schema({
	name: {
		type: String,
		required: [true, "name is required"],
		validate: [
			(value) => {
				if (value) return /^[A-Za-z ]+$/.test(value);
				else value;
			},
			"name can contain only letters",
		],
		set: (value) => {
			if (value)
				return value
					.trim()
					.split(" ")
					.map((w) => w[0].toUpperCase() + w.substr(1).toLowerCase())
					.join(" ");
		},
	},
	address: {
		type: String,
		required: [true, "address is required"],
	},
	city: {
		type: String,
		required: [true, "city name is required"],
		validate: [
			(value) => {
				if (value) return /^[A-Za-z ]+$/.test(value);
				else value;
			},
			"city name can contain only letters",
		],
		lowercase: true,
	},
	state: {
		type: String,
		required: [true, "state name is required"],
		validate: [
			(value) => {
				if (value) return /^[A-Za-z ]+$/.test(value);
				else value;
			},
			"state name can contain only letters",
		],
		lowercase: true,
	},
	phone: {
		type: String,
		trim: true,
		required: [true, "phone number is required"],
		validate: [
			(phone) => {
				let regxp = /^\d{10}$/;
				return regxp.test(phone);
			},
			"invalid phone number",
		],
	},
	ratings: {
		type: Number,
		required: [true, "dealer ratings is required"],
	},
});

DealerSchema.plugin(ValidationError, {
	capitalize: false,
	transform: function (messages) {
		return messages[0].toLowerCase();
	},
});

DealerSchema.plugin(UniqueValidation);

module.exports = mongoose.model("Dealer", DealerSchema);
