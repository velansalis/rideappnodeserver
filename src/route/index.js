const express = require("express");
const router = express.Router();
const path = require("path");

const { HttpException } = require("../types/index");

router.use("/images", express.static(path.resolve(__dirname, "../../uploads")));
router.use("/docs", express.static(path.resolve(__dirname, "../../docs")));

router.use("/auth", require("./router/auth.router"));
router.use("/trip", require("./router/trip.router"));
router.use("/rider", require("./router/rider.router"));
router.use("/service", require("./router/service.router"));
router.use("/dealer", require("./router/dealer.router"));

router.use((req, res, next) => {
	let { data } = res;

	if (!data) {
		res.status(404);
		let routeError = new HttpException("NotFound", "route not found");
		next(routeError);
	} else {
		next();
	}
});

module.exports = router;
