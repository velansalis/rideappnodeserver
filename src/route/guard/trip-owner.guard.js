const { Trip } = require("../../model/index");

const { HttpException } = require("../../types/index");

const tripOwnerGuard = async (req, res, next) => {
	const owner = req.token._id;
	const { tripID } = req.params;
	let trip = null;

	try {
		if (!Trip.isValid(tripID)) throw new Error("invalid trip id");
		trip = await Trip.findOne({ _id: tripID }).lean().exec();
		if (!trip) throw new HttpException("UnauthorizedAccess", "trip doesn't exists");
		if (trip.owner != owner) throw new HttpException("UnauthorizedAccess", "you don't own this trip");
		next();
	} catch (err) {
		next(err);
	}
};

module.exports = { tripOwnerGuard };
