const { Rider } = require("../../model/index");

const { tokenHandler } = require("../../lib/index").shared;

const { HttpException } = require("../../types/index");

const checkRidersExistence = async (_id) => {
	return new Promise(async (resolve, reject) => {
		let rider = await Rider.findOne({ _id }).exec();
		if (rider) resolve();
		else reject(new HttpException("UserNotExists", "rider doesn't exists"));
	});
};

const tokenGuard = async (req, res, next) => {
	try {
		const { authorization } = req.headers;
		const token = authorization ? authorization.split(" ")[1] : "";
		req.token = await tokenHandler.verifyJwt(token);
		await checkRidersExistence(req.token._id);
		next();
	} catch (err) {
		if (err instanceof HttpException) next(err);
		else next(new HttpException(err.name));
	}
};

module.exports = { tokenGuard };
