const router = require("express").Router();

const { tokenGuard } = require("../guard/token.guard");
const { addService, getService, getServices } = require("../../module/index").service;

router.post("/", tokenGuard, addService);
router.get("/", tokenGuard, getServices);
router.get("/:serviceID", tokenGuard, getService);

module.exports = router;
