const router = require("express").Router();

const { avatarUploadHandler } = require("../../lib/shared/file-upload.handler");

const { editRider, getRider, addAvatar, getRidersByContact } = require("../../module/rider/rider.module");
const { tokenGuard } = require("../guard/token.guard");

router.get("/", tokenGuard, getRider);
router.post("/contacts", tokenGuard, getRidersByContact);
router.put("/", tokenGuard, editRider);
router.post("/avatar", tokenGuard, avatarUploadHandler, addAvatar);

module.exports = router;
