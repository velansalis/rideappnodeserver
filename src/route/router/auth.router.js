const router = require("express").Router();

const {
	loginController,
	registerController,
	terminateController,
	forgotPasswordController,
	resetPasswordController,
	verifyOTP,
	resetOTP,
	refreshToken,
} = require("../../module/index").auth;

const { tokenGuard } = require("../guard/token.guard");

router.post("/login", loginController);
router.post("/register", registerController);
router.post("/terminate", terminateController);
router.post("/reset-password", tokenGuard, resetPasswordController);
router.post("/forgot-password", forgotPasswordController);
router.post("/otp/verify", verifyOTP);
router.post("/otp/reset", resetOTP);
router.post("/oauth/refresh", refreshToken);

module.exports = router;
