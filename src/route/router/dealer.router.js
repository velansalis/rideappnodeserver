const router = require("express").Router();
const { tokenGuard } = require("../guard/token.guard");
const { getDealers, addDealer, getDealer } = require("../../module/index").dealer;

router.post("", tokenGuard, addDealer);
router.get("", tokenGuard, getDealers);
router.get("/:dealerID", tokenGuard, getDealer);

module.exports = router;
