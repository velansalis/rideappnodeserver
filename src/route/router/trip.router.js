const router = require("express").Router();

const { tokenGuard } = require("../guard/token.guard");
const { tripOwnerGuard } = require("../guard/trip-owner.guard");
const { getTrip, getTrips, addTrip, editTrip, deleteTrip } = require("../../module/index").trip;

router.get("/", tokenGuard, getTrips);
router.get("/:tripID", tokenGuard, getTrip);
router.post("/", tokenGuard, addTrip);
router.put("/:tripID", tokenGuard, tripOwnerGuard, editTrip);
router.delete("/:tripID", tokenGuard, tripOwnerGuard, deleteTrip);

module.exports = router;
