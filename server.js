const server = require("./src/lib/util/server.handler");
const mongoose = require("./src/lib/util/mongoose.handler");
const environment = require("./src/lib/util/environment.handler");

const PORT = process.env.PORT || 3000;

(async () => {
	try {
		await environment.loadEnvironmentVariables();
		await mongoose.connectMongoose();
		await server.startServer(PORT);
	} catch (err) {
		console.log(err);
		mongoose.disconnectMongoose();
		server.closeServer();
	}
})();

process.on("SIGINT", () => {
	console.log();
	mongoose.disconnectMongoose();
	server.closeServer();
	process.exit();
});
