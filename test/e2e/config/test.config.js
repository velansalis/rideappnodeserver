const folder = process.argv[2];

const collection = {
	e2eAutomation: "https://www.getpostman.com/collections/220c83cbcab6e75e1414",
};

const environment = {
	development: `${__dirname}/dev.environment.json`,
};

const options = {
	collection: collection.e2eAutomation,
	reporters: "cli",
	environment: environment.development,
	folder: folder ? folder : undefined,
	bail: true,
};

module.exports = options;
