const newman = require("newman");
const options = require("./config/test.config");

const { loadEnvironmentVariables } = require("../../src/lib/util/environment.handler");
const { connectMongoose } = require("../../src/lib/util/mongoose.handler");
const { startServer } = require("../../src/lib/util/server.handler");

const PORT = 3001;

const startTest = async (options) => {
	await loadEnvironmentVariables();
	await connectMongoose();
	await startServer(PORT);

	try {
		newman.run(options, (err) => {
			if (err) throw err;
			process.exit();
		});
	} catch (err) {
		console.log(err);
	}
};

startTest(options);
